const Discord = require('discord.js');
const client = new Discord.Client({
	presence: {
		status: 'online',
		activity: {
			name: `Monitoring Depresso Esspresso`,
			type: 'PLAYING'
		}
	}
});
const Enmap = require('enmap');
const EnmapMongo = require('enmap-mongo');

module.exports = class Client {
	constructor() {
		this.commandMap;
	}
	static get bot() {
		return client;
	}
	static get Discord() {
		return Discord;
	}
	static async initiate() {
		client.login(process.env.TOKEN);
		console.log('-_-_- Depresso Esspresso Starting... -_-_-'.green);
		await console.log(' ');
		//return enmap
		return new EnmapMongo({
			name: 'Depresso Espresso',
			dbName: 'Depresso Esspresso',
			host: process.env.HOST
		});
	}
	set commands(commands) {
		this.commandMap = commands;
	}
};
