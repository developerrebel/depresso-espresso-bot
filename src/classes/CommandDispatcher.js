const errorMessages = require("../util/errorMessages")
module.exports = class CommandDispatcher {
    static handle(commandData, command) {
        if(command.permissionReq) {
            if(!commandData.message.member.hasPermission(command.permissionReq)) return commandData.send(errorMessages.improperPermission(command.permissionReq))
        }
        command.execute.call(commandData, commandData.message, commandData.Discord, commandData.args, commandData.bot)
    }
}