const { EventEmitter } = require("events")

module.exports = class EventHandler extends EventEmitter{
    /**
     * @param {String} event The event you want to emit
     * @param {any} eventInfo The info you want to be passed along with the event
     * @fires EventHandler#event fires the the event of your choosing
     */
    static emitEvent(event, eventInfo) {
        this.emit(event, eventInfo)
    }
}