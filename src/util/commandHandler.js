const fs = require('fs');
const path = require('path');
const Discord = require('discord.js');
const commands = new Discord.Collection();
const commandListener = require('../listeners/message');
module.exports.start = async (Client, client, enmap) => {
	let files = [
		{
			directoryPath: path.join(process.cwd(), '/src/commands'),
			files: fs.readdirSync(path.join(process.cwd(), '/src/commands')).filter(f => f.endsWith('.js'))
		}
	];
	try {
		extraDirectories = fs.readdirSync(path.join(process.cwd(), '/src/commands')).filter(f => !f.endsWith('.js'));
		extraDirectories.forEach(dir => {
			files.push({
				directoryPath: path.join(process.cwd(), '/src/commands/', dir),
				files: fs.readdirSync(path.join(process.cwd(), '/src/commands/', dir)).filter(f => f.endsWith('.js'))
			});
		});
		console.log('-_-_- Loading Commands -_-_-'.magenta);
		console.log(' ');
	} catch {}
	await files.forEach(async f => {
		if (f.files.length === 0) return console.log('No files to be loaded!'.red);
		f.files.forEach(file => {
			const Command = require(path.join(f.directoryPath, file));
			const command = new Command();
			const fileName = file.split('.')[0];
			console.log(`${command.name || fileName} loaded from ${f.directoryPath.split('\\').pop()}`.cyan);
			commands.set(command.name || fileName, command);
			client.commands = commands;
		});
	});
	commandListener.start(Client.bot, client.commandMap, Client, client, enmap);
	console.log('-_-_- Finished Loading Commands -_-_-'.magenta);
	console.log(' ');
	console.log('-_-_- Depresso Esspresso Started -_-_-'.green);
	console.log(' ');
};
