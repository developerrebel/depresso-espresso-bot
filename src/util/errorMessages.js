const Discord = require("discord.js")

const improperPermission = (permission) => {
    const embed = new Discord.MessageEmbed()
    .setTitle("Error")
    .setColor("e50909")
    .setDescription(`You need the **${permission.toLowerCase()}** permission to use this command!`)
    return embed
}

const noChannelFound = (channel) => {
    const embed = new Discord.MessageEmbed()
    .setTitle("Error")
    .setColor("e50909")
    .setDescription(`Could not find the channel __**${channel}**__ in this guild. Please make sure you provided the correct channel name or id and try again.`)
    return embed
}

module.exports = {
    improperPermission,
    noChannelFound
}