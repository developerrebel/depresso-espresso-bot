const errorMessages = require('../util/errorMessages');
const CommandDispatcher = require('../classes/CommandDispatcher');
module.exports.start = async (bot, commands, Client, client, enmap) => {
	bot.on('message', message => {
		const prefix = process.env.PREFIX;
		if (!message.content.startsWith(prefix)) return;
		const messageArray = message.content.slice(prefix.length).split(' ');
		const args = messageArray.slice(1);
		const cmd = messageArray[0];

		const commandData = {
			voice: message.member.voice,
			botVoice: message.guild.me.voice,
			send: sendMessage => message.channel.send(sendMessage),
			Discord: Client.Discord,
			commands: client.commandMap,
			message,
			bot,
			args,
			errorMessages,
			provider: enmap
		};
		const command = commands.get(cmd);
		if (!command) return;
		CommandDispatcher.handle(commandData, command);
	});

	await console.log(' ');
	await console.log('-_-_- Command Listener Started -_-_-'.green);
	console.log(' ');
};
