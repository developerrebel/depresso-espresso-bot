const { Structures } = require('discord.js');
const enmap = require('enmap');
module.exports.create = enmapProvider => {
	Structures.extend('GuildMember', guildMember => {
		class extendedGuildMember extends guildMember {
			constructor(client, data, guild) {
				super(client, data, guild);
				this.options = new enmap({ name: 'guildMemberOptions', provider: enmapProvider });
			}
		}
		return extendedGuildMember;
	});
};
