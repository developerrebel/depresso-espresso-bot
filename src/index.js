require('dotenv').config();
require('colors');
const Client = require('./classes/Client');
const client = new Client();
const commandHandler = require('./util/commandHandler');
const guildMemberStructure = require('./structures/guildMember');
Client.initiate().then(enmap => {
	commandHandler.start(Client, client, enmap);
	guildMemberStructure.create(enmap);
});
